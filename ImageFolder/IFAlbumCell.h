//
//  IFAlbumCell.h
//  ImageFolder
//
//  Created by Shashank Garg on 06/10/16.
//  Copyright © 2016 inzak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IFAlbumCell : UITableViewCell

- (void)setName:(NSString *)albumName
      withCount:(NSInteger)albumCount
      withImage:(UIImage *)image;

@end
