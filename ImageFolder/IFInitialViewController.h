//
//  IFInitialViewController.h
//  ImageFolder
//
//  Created by Shashank Garg on 20/09/16.
//  Copyright © 2016 inzak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IFInitialViewController : UIViewController

@property (strong, nonatomic) NSString *albumName;

@end
