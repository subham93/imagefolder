//
//  AppDelegate.h
//  ImageFolder
//
//  Created by Shashank Garg on 17/09/16.
//  Copyright © 2016 inzak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

