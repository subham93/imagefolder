//
//  IFAlbumCell.m
//  ImageFolder
//
//  Created by Shashank Garg on 06/10/16.
//  Copyright © 2016 inzak. All rights reserved.
//

#import "IFAlbumCell.h"

@interface IFAlbumCell()

@property (weak, nonatomic) IBOutlet UILabel *albumNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *albumCount;
@property (weak, nonatomic) IBOutlet UIImageView *albumImage;

@end


@implementation IFAlbumCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setName:(NSString *)albumName
      withCount:(NSInteger)albumCount
      withImage:(UIImage *)image {
    
    [_albumImage setImage:image];
    [_albumCount setText:[NSString stringWithFormat:@"%ld", (long)albumCount]];
    [_albumNameLabel setText:albumName];
}

@end
