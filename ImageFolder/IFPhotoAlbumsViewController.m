//
//  IFPhotoAlbumsViewController.m
//  ImageFolder
//
//  Created by Shashank Garg on 06/10/16.
//  Copyright © 2016 inzak. All rights reserved.
//

#import "IFPhotoAlbumsViewController.h"
#import <Photos/Photos.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "IFAlbumCell.h"
#import "IFInitialViewController.h"
#import "IFAssetEditViewController.h"

@interface IFPhotoAlbumsViewController ()

@property (strong, nonatomic) NSMutableArray *albumArr;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation IFPhotoAlbumsViewController
{
    PHCachingImageManager *cachingImageManager;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _albumArr = [NSMutableArray array];
    cachingImageManager = [[PHCachingImageManager alloc] init];
    
    PHFetchResult *userAlbums = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum subtype:PHAssetCollectionSubtypeAny options:nil];
    PHFetchResult *smartAlbums = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum subtype:PHAssetCollectionSubtypeAny options:nil];
    
    [smartAlbums enumerateObjectsUsingBlock:^(PHAssetCollection *collection, NSUInteger idx, BOOL *stop) {
        __block NSMutableDictionary *albumDict = [NSMutableDictionary dictionary];
        PHFetchResult *collectionResult = [PHAsset fetchAssetsInAssetCollection:collection options:nil];
        
        PHAsset *asset = [collectionResult firstObject];
        
        if (collection.localizedTitle && [NSNumber numberWithUnsignedInteger:collectionResult.count] && asset) {
            [albumDict setObject:collection.localizedTitle forKey:@"albumName"];
            [albumDict setObject:[NSNumber numberWithUnsignedInteger:collectionResult.count] forKey:@"albumCount"];
            
            if (asset) {
                [albumDict setObject:asset forKey:@"firstAsset"];
            }
            if (collectionResult.count != 0 && asset)
                [_albumArr addObject:albumDict];
        }
    }];
    
    [userAlbums enumerateObjectsUsingBlock:^(PHAssetCollection *collection, NSUInteger idx, BOOL *stop) {
        __block NSMutableDictionary *albumDict = [NSMutableDictionary dictionary];
        PHFetchResult *collectionResult = [PHAsset fetchAssetsInAssetCollection:collection options:nil];
        
        PHAsset *asset = [collectionResult firstObject];
        
        if (collection.localizedTitle && [NSNumber numberWithUnsignedInteger:collectionResult.count] && asset) {
            [albumDict setObject:collection.localizedTitle forKey:@"albumName"];
            [albumDict setObject:[NSNumber numberWithUnsignedInteger:collectionResult.count] forKey:@"albumCount"];
            
            if (asset) {
                [albumDict setObject:asset forKey:@"firstAsset"];
            }
            if (collectionResult.count != 0 && asset)
                [_albumArr addObject:albumDict];
        }
    }];
    
    NSArray *albumAssetArr = [_albumArr valueForKeyPath:@"firstAsset"];
    [cachingImageManager startCachingImagesForAssets:albumAssetArr targetSize:CGSizeMake(100, 100) contentMode:PHImageContentModeAspectFit options:nil];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 66;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [_albumArr count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self getImgAndVideoBtnClickedWithName:[_albumArr[indexPath.row] valueForKey:@"albumName"]];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellId = @"IFAlbumCell";
    
    IFAlbumCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellId owner:self options:nil];
        cell = (IFAlbumCell *)[nib objectAtIndex:0];
    }
    __block NSDictionary *eachAlbumDict = _albumArr[indexPath.row];
    NSArray *eachAlbumAsset = [_albumArr valueForKeyPath:@"firstAsset"];
    [cachingImageManager requestImageForAsset:eachAlbumAsset[indexPath.row] targetSize:CGSizeMake(100, 100) contentMode:PHImageContentModeAspectFit options:nil resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
        [cell setName:[eachAlbumDict valueForKey:@"albumName"] withCount:[[eachAlbumDict valueForKey:@"albumCount"] integerValue] withImage:result];
    }];
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getImgAndVideoBtnClickedWithName:(NSString *)name {
    
    IFAssetEditViewController *initialVC = [[IFAssetEditViewController alloc] initWithNibName:@"IFInitialViewController" bundle: nil];
    initialVC.albumName = name;
    [self.navigationController pushViewController:initialVC animated:YES];
}

@end
