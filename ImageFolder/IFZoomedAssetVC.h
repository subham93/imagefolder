//
//  IFZoomedPhotoVC.h
//  ImageFolder
//
//  Created by Shashank Garg on 18/09/16.
//  Copyright © 2016 inzak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "IFPhotoManager.h"


@protocol IFZoomedPhotoVCDelegate <NSObject>

- (void)deletePhotoTappedForIndexPath:(NSIndexPath *)indexPath;

@end

@interface IFZoomedAssetVC : UIViewController

@property (strong, nonatomic) UIImage *image;
@property (strong, nonatomic) AVPlayerItem *playerItem;
@property (strong, nonatomic) NSIndexPath *indexPath;
@property (strong, nonatomic) ALAsset *videoItem;
@property (weak, nonatomic) id delegate;
@property (weak, nonatomic) IFPhotoManager *photoManager;
@property (weak, nonatomic) id asset;

@end
