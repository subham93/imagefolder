//
//  IFZoomedPhotoVC.m
//  ImageFolder
//
//  Created by Shashank Garg on 18/09/16.
//  Copyright © 2016 inzak. All rights reserved.
//

#import "IFZoomedAssetVC.h"
#import <MediaPlayer/MediaPlayer.h>

#define SYSTEM_VERSION_LESS_THAN(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

@interface IFZoomedAssetVC ()
@property(nonatomic,strong) MPMoviePlayerViewController *moviePlayerController;

@end

@implementation IFZoomedAssetVC
{
    AVPlayer *avPlayer;
    AVPlayerLayer *avPlayerLayer;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    if (!SYSTEM_VERSION_LESS_THAN(@"8.0"))
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(deletePhoto)];
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"dropleft"] style:UIBarButtonItemStyleDone target:self action:@selector(backButtonTapped)];
    
    
}

-(void)viewDidAppear:(BOOL)animated {
    if (_image) {
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:self.view.frame];
        [imgView setImage:_image];
        imgView.contentMode = UIViewContentModeScaleAspectFit;
        imgView.clipsToBounds = YES;
        
        [self.view addSubview:imgView];
        
    } else if (_playerItem) {
        avPlayer = [[AVPlayer alloc] initWithPlayerItem:_playerItem];
        avPlayerLayer =[AVPlayerLayer playerLayerWithPlayer:avPlayer];
        [avPlayerLayer setFrame:self.view.frame];
        [self.view.layer addSublayer:avPlayerLayer];
        [avPlayer seekToTime:kCMTimeZero];
        [avPlayer play];
    }
    else if (_videoItem) {
        self.moviePlayerController = [[MPMoviePlayerViewController alloc]initWithContentURL:_videoItem.defaultRepresentation.url];
        self.moviePlayerController .view.frame = self.view.bounds; //Set the size
        [self.view addSubview:self.moviePlayerController .view]; //Show the view
        [self.moviePlayerController .moviePlayer play]; //Start playing
    }
}

- (void)backButtonTapped {
    
    [self dismissVC];
}

- (void)dismissVC {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)deletePhoto {
    
    
    [self.photoManager deletePhoto:self.asset withCompletionHandler:^(BOOL success, NSError * _Nullable error) {
        if (success) {
            [self dismissVC];
            if (_delegate && [_delegate respondsToSelector:@selector(deletePhotoTappedForIndexPath:)]) {
                
                [_delegate deletePhotoTappedForIndexPath:_indexPath];
            }
        }
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
