//
//  IFDemoViewController.m
//  ImageFolder
//
//  Created by Shashank Garg on 22/09/16.
//  Copyright © 2016 inzak. All rights reserved.
//

#import "IFDemoViewController.h"
#import "IFPhotoManager.h"
#import "IFInitialViewController.h"
#import "IFPhotoAlbumsViewController.h"

#define SYSTEM_VERSION_LESS_THAN(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

@interface IFDemoViewController ()

@end

@implementation IFDemoViewController
{
    NSString *customAlbumName;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    customAlbumName = @"testTitle6";;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)saveImgBtnClicked:(id)sender {
    
    // POC: Add Photo And Video.
    [self checkPhotosStatus];
    IFPhotoManager *photoManager = [[IFPhotoManager alloc] init];
    [photoManager saveImage:[UIImage imageNamed:@"logo2"] toCustomAlbum:customAlbumName completionHandler:^(BOOL success, NSError * _Nullable error) {
        
    }];
}

- (IBAction)saveVideoBtnClicked:(id)sender {
    
    [self checkPhotosStatus];
    IFPhotoManager *photoManager = [[IFPhotoManager alloc] init];
    NSString *videoPath = [[NSBundle mainBundle] pathForResource:@"Chat1" ofType:@"mp4"];
    [photoManager saveVideo:videoPath toCustomAlbum:customAlbumName completionHandler:^(BOOL success, NSError * _Nullable error) {
        
    }];
}

- (IBAction)getImgAndVideoBtnClicked:(id)sender {
    
//    [self checkPhotosStatus];
    IFInitialViewController *initialVC = [[IFInitialViewController alloc] initWithNibName:@"IFInitialViewController" bundle: nil];
    initialVC.albumName = customAlbumName;
    [self.navigationController pushViewController:initialVC animated:YES];
}

- (IBAction)showAlbums:(id)sender {
    
    IFPhotoAlbumsViewController *initialVC = [[IFPhotoAlbumsViewController alloc] initWithNibName:@"IFPhotoAlbumsViewController" bundle: nil];
    [self.navigationController pushViewController:initialVC animated:YES];
}

- (void)checkPhotosStatus {
    
    if (!SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        if ([PHPhotoLibrary authorizationStatus] == PHAuthorizationStatusDenied) {
            NSLog(@"Handle Denied Here");
        } else if ([PHPhotoLibrary authorizationStatus] == PHAuthorizationStatusRestricted) {
            NSLog(@"Handle Restricted Here");
        }
    } else {
        if ([ALAssetsLibrary authorizationStatus] == ALAuthorizationStatusDenied) {
            NSLog(@"Handle Denied Here");
        } else if ([ALAssetsLibrary authorizationStatus] == ALAuthorizationStatusRestricted) {
            NSLog(@"Handle Restricted Here");
        }
    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
