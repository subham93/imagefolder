//
//  IFPhotoManager.h
//  ImageFolder
//
//  Created by Shashank Garg on 18/09/16.
//  Copyright © 2016 inzak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Photos/Photos.h>
#import <AssetsLibrary/AssetsLibrary.h>

NS_ASSUME_NONNULL_BEGIN

@protocol IFPhotoManagerDelegate <NSObject>

- (void)reloadCollectionView;

@end

@interface IFPhotoManager : NSObject

// Public Methods to Save Photo and Video
- (void) saveImage:(UIImage *)image
     toCustomAlbum:(NSString *)albumName
 completionHandler:(nullable void(^)(BOOL success, NSError *__nullable error))completionHandler;


- (void) saveVideo:(NSString *)videoUrl
     toCustomAlbum:(NSString *)albumName
 completionHandler:(nullable void(^)(BOOL success, NSError *__nullable error))completionHandler;


- (void)getAssetsInAlbum:(NSString *)albumName
       completionHandler:(nullable void(^)(NSMutableArray *assetArray, NSMutableArray *contentArray, NSError *__nullable error))completionHandler;


- (void)deletePhoto:(PHAsset *)asset
withCompletionHandler:(nullable void(^)(BOOL success, NSError *__nullable error))completionHandler;


@end



NS_ASSUME_NONNULL_END
