//
//  IFInitialViewController.m
//  ImageFolder
//
//  Created by Shashank Garg on 20/09/16.
//  Copyright © 2016 inzak. All rights reserved.
//

#import "IFInitialViewController.h"
#import "IFPhotoManager.h"
#import "IFZoomedAssetVC.h"
#import <AssetsLibrary/AssetsLibrary.h>

#define SYSTEM_VERSION_LESS_THAN(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define COLLECTION_CELL_SPACING         10.0
#define COLUMN_IN_COLLETIONVIEW         3

typedef enum {SCROLLING_UP, SCROLLING_DOWN}SCROLL_DIRECTION;

@interface IFInitialViewController () <IFPhotoManagerDelegate, IFZoomedPhotoVCDelegate>

@property (assign, nonatomic) CGFloat lastContentOffset;
@property (assign, nonatomic) SCROLL_DIRECTION scrollDirection;

@end


static NSString *collectionCellId = @"collectionCellId";

@implementation IFInitialViewController
{
    PHPhotoLibrary *library;
    NSMutableArray *detailedAssetArr;
    PHCachingImageManager *cachingImageManager;
    
    PHAsset *selectedAsset;
    IFPhotoManager *photoManager;
    ALAssetsLibrary *assetLib;
    __weak IBOutlet UICollectionView *imageCollectionView;
    
    NSInteger lastFromCachedIndex;
    NSInteger lastToCachedIndex;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    // Initialize iVars and Photo Manager.
    detailedAssetArr = [NSMutableArray array];
    _scrollDirection = SCROLLING_DOWN;
    
    photoManager = [[IFPhotoManager alloc] init];
    cachingImageManager = [[PHCachingImageManager alloc] init];
    
    [self configureCollectionView];
    
    [self getAssets];
}

-(void)dealloc {
    
    imageCollectionView.delegate = nil;
}

- (void)getAssets {
    
    [detailedAssetArr removeAllObjects];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [photoManager getAssetsInAlbum:_albumName completionHandler:^(NSMutableArray * _Nonnull assetArray, NSMutableArray * _Nonnull contentArray, NSError * _Nullable error) {
            if (error) {
                NSLog(@"Error : %@", error);
            }
            for (int i=0; i<assetArray.count; i++) {
                NSMutableDictionary *dict = [NSMutableDictionary dictionary];
                [dict setObject:assetArray[i] forKey:@"asset"];
                [detailedAssetArr addObject:dict];
            }
            NSInteger toIndex = detailedAssetArr.count > 2*[self getNumberOfImagesInFrame] ? 2*[self getNumberOfImagesInFrame] : detailedAssetArr.count;
            [self startCachingFromIndex:0 toIndex:toIndex];
            
            // Create Video Thumbnail
            [self createThumbnail];
            dispatch_async(dispatch_get_main_queue(), ^{
                [imageCollectionView reloadData];
            });
        }];
    });
}

- (NSInteger)getNumberOfImagesInFrame {
    
    return COLUMN_IN_COLLETIONVIEW * self.view.frame.size.height/(self.view.frame.size.width/3);
}

- (void)createThumbnail {
    
    //    NSArray *contentArr = [detailedAssetArr valueForKeyPath:@"content"];
    NSArray *assetArr = [detailedAssetArr valueForKeyPath:@"asset"];
    
    [assetArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (![obj isKindOfClass:[UIImage class]]) {
            
            // For iOS7
            if ([obj isKindOfClass:[ALAsset class]]) {
                UIImageOrientation orientation =
                (UIImageOrientation)[[obj valueForProperty:@"ALAssetPropertyOrientation"] intValue];
                UIImage *img = [UIImage imageWithCGImage:[[obj defaultRepresentation] fullScreenImage]
                                                   scale:1.0
                                             orientation:orientation];
                NSMutableDictionary *dict = [detailedAssetArr objectAtIndex:idx];
                [dict setObject:img forKey:@"thumbnail"];
            }
        }
    }];
}

- (void)configureCollectionView {
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    [imageCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:collectionCellId];
    [imageCollectionView setBackgroundColor:[UIColor whiteColor]];
    
    imageCollectionView.delegate = (id)self;
    imageCollectionView.dataSource = (id)self;
    imageCollectionView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:imageCollectionView];
}

#pragma mark collection view Delegates
- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(COLLECTION_CELL_SPACING, COLLECTION_CELL_SPACING, COLLECTION_CELL_SPACING, COLLECTION_CELL_SPACING); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return COLLECTION_CELL_SPACING;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return [detailedAssetArr count];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:collectionCellId forIndexPath:indexPath];
    
    // Reset Cell contents.
    for (UIView *subViews in cell.subviews) {
        [subViews removeFromSuperview];
    }
    
    [self performCachingForIndex:indexPath.row];
    
    NSArray *assetArr = [detailedAssetArr valueForKeyPath:@"asset"];
    __block UIImage *img;
    if ([[[detailedAssetArr objectAtIndex:indexPath.row] valueForKey:@"thumbnail"] isKindOfClass:[UIImage class]])
        img = [[detailedAssetArr objectAtIndex:indexPath.row] valueForKey:@"thumbnail"];
    else if ([[[detailedAssetArr objectAtIndex:indexPath.row] valueForKey:@"asset"] isKindOfClass:[UIImage class]])
        img = [[detailedAssetArr objectAtIndex:indexPath.row] valueForKey:@"asset"];
    else {
        [cachingImageManager requestImageForAsset:assetArr[indexPath.row] targetSize:CGSizeMake(200, 200) contentMode:PHImageContentModeAspectFill options:nil resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
            img = result;
        }];
    }
    
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height)];
    imgView.contentMode = UIViewContentModeScaleAspectFill;
    imgView.clipsToBounds = YES;
    imgView.image = img;
    [cell addSubview:imgView];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    double size = self.view.frame.size.width/COLUMN_IN_COLLETIONVIEW-COLLECTION_CELL_SPACING*(COLUMN_IN_COLLETIONVIEW+1)/COLUMN_IN_COLLETIONVIEW;
    return CGSizeMake(size, size);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    IFZoomedAssetVC *zoomedVC = [[IFZoomedAssetVC alloc] init];
    if ([[[detailedAssetArr objectAtIndex:indexPath.row] valueForKey:@"asset"] isKindOfClass:[UIImage class]]) {
        zoomedVC.image = [[detailedAssetArr objectAtIndex:indexPath.row] valueForKey:@"asset"];
    }
    
    else if ([[[detailedAssetArr objectAtIndex:indexPath.row] valueForKey:@"asset"] isKindOfClass:[ALAsset class]]) {
        zoomedVC.videoItem = [[detailedAssetArr objectAtIndex:indexPath.row] valueForKey:@"asset"];
    } else {
        
        PHAsset *asset = [[detailedAssetArr objectAtIndex:indexPath.row] valueForKey:@"asset"];
        PHImageManager *manager = [PHImageManager defaultManager];
        PHImageRequestOptions *imageRequestOptions = [[PHImageRequestOptions alloc] init];
        imageRequestOptions.resizeMode   = PHImageRequestOptionsResizeModeFast;
        imageRequestOptions.deliveryMode = PHImageRequestOptionsDeliveryModeFastFormat;
        imageRequestOptions.synchronous = YES;
        
        PHVideoRequestOptions *videoRequestOptions = [[PHVideoRequestOptions alloc] init];
        videoRequestOptions.version = PHVideoRequestOptionsVersionOriginal;
        videoRequestOptions.deliveryMode = PHVideoRequestOptionsDeliveryModeFastFormat;
        
        if (asset.mediaType == PHAssetMediaTypeImage) {
            [manager requestImageForAsset:asset
                               targetSize:PHImageManagerMaximumSize
                              contentMode:PHImageContentModeDefault
                                  options:imageRequestOptions
                            resultHandler:^void(UIImage *image, NSDictionary *info) {
                                zoomedVC.image = image;
                            }];
        } else if (asset.mediaType == PHAssetMediaTypeVideo) {
            [manager requestPlayerItemForVideo:asset
                                       options:videoRequestOptions
                                 resultHandler:^(AVPlayerItem * _Nullable playerItem, NSDictionary * _Nullable info) {
                                     zoomedVC.playerItem = playerItem;
                                 }];
        }
    }
    
    zoomedVC.delegate = self;
    zoomedVC.indexPath = indexPath;
    zoomedVC.photoManager = photoManager;
    zoomedVC.asset = [[detailedAssetArr objectAtIndex:indexPath.row] valueForKey:@"asset"];
    //    UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:zoomedVC];
    [self.navigationController pushViewController:zoomedVC animated:YES];
}

- (void)deletePhotoTappedForIndexPath:(NSIndexPath *)indexPath {
    
    if (![[[detailedAssetArr objectAtIndex:indexPath.row] valueForKey:@"asset"] isKindOfClass:[PHAsset class]])
        return;
    
    PHAsset *asset = [[detailedAssetArr objectAtIndex:indexPath.row] valueForKey:@"asset"];
    if (asset == nil)
        return;
    
    [detailedAssetArr removeObjectAtIndex:indexPath.row];
    [cachingImageManager stopCachingImagesForAllAssets];
    [self performCachingForIndex:indexPath.row];
    [imageCollectionView reloadData];
}

- (void)reloadCollectionView {
    
    [imageCollectionView reloadData];
}


- (UIImage *)screenshotForAsset:(AVAsset *)asset maximumSize:(CGSize)maxSize {
    
    CMTime actualTime;
    NSError *error;
    
    AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    
    // Setting a maximum size is not necessary
    
    CGImageRef cgIm = [generator copyCGImageAtTime:CMTimeMake(1, 10)
                                        actualTime:&actualTime
                                             error:&error];
    UIImage *image = [UIImage imageWithCGImage:cgIm];
    CFRelease(cgIm);
    
    if (error) {
        NSLog(@"Error making screenshot: %@", [error localizedDescription]);
        NSLog(@"Actual screenshot time: %f Requested screenshot time: %f", CMTimeGetSeconds(actualTime),
              CMTimeGetSeconds(CMTimeMake(1, 1)));
        return nil;
    }
    
    return image;
}

- (UIImage *)createScreenshotForVideoAtIndex:(NSInteger)row forItem:(id)content {
    
    if ([content isKindOfClass:[ALAsset class]]) {
        UIImageOrientation orientation =
        (UIImageOrientation)[[content valueForProperty:@"ALAssetPropertyOrientation"] intValue];
        return [UIImage imageWithCGImage:[[content defaultRepresentation] fullScreenImage]
                                   scale:1.0
                             orientation:orientation];
    } else {
        return [self screenshotForAsset:((AVPlayerItem *)content).asset maximumSize:CGSizeMake(400, 400)];
    }
    
}


#pragma mark Caching

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (!self)
        return;
    
    if (self.lastContentOffset > scrollView.contentOffset.y)
    {
        NSLog(@"Scrolling Up");
        _scrollDirection = SCROLLING_UP;
    }
    else if (self.lastContentOffset < scrollView.contentOffset.y)
    {
        NSLog(@"Scrolling Down");
        _scrollDirection = SCROLLING_DOWN;
    }
    
    self.lastContentOffset = scrollView.contentOffset.y;
}

- (void)performCachingForIndex:(NSInteger)index {
    
    if (lastToCachedIndex < index + [self getNumberOfImagesInFrame]/2 && _scrollDirection == SCROLLING_DOWN && lastToCachedIndex != detailedAssetArr.count-1) {
        NSInteger fromIndex = index - [self getNumberOfImagesInFrame] > 0 ? index - [self getNumberOfImagesInFrame] : 0;
        NSInteger toIndex = index + [self getNumberOfImagesInFrame] > detailedAssetArr.count ? detailedAssetArr.count-1 : index + [self getNumberOfImagesInFrame];
        [self startCachingFromIndex:fromIndex toIndex:toIndex];
    }
    if (lastFromCachedIndex > index - [self getNumberOfImagesInFrame]/2 && _scrollDirection == SCROLLING_UP && lastFromCachedIndex > 0) {
        NSInteger fromIndex = index - [self getNumberOfImagesInFrame] > 0 ? index - [self getNumberOfImagesInFrame] : 0;
        NSInteger toIndex = index + [self getNumberOfImagesInFrame] > detailedAssetArr.count ? detailedAssetArr.count-1 : index + [self getNumberOfImagesInFrame];
        [self startCachingFromIndex:fromIndex toIndex:toIndex];
    }
}

- (void)startCachingFromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex {
    
    [cachingImageManager stopCachingImagesForAllAssets];
    lastFromCachedIndex = fromIndex;
    lastToCachedIndex = toIndex;
    
    NSArray *assetArr = [detailedAssetArr valueForKeyPath:@"asset"];
    NSArray *cachedArr = [assetArr objectsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(fromIndex, toIndex-fromIndex)]];
    [cachingImageManager startCachingImagesForAssets:cachedArr targetSize:CGSizeMake(200, 200) contentMode:PHImageContentModeAspectFill options:nil];
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    [cachingImageManager stopCachingImagesForAllAssets];
}

@end
