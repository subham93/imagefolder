//
//  IFPhotoManager.m
//  ImageFolder
//
//  Created by Shashank Garg on 18/09/16.
//  Copyright © 2016 inzak. All rights reserved.
//

#import "IFPhotoManager.h"

#define SYSTEM_VERSION_LESS_THAN(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)


@interface ALAssetsLibrary (CustomPhotoAlbum)

- (void)saveImage:(UIImage *)image
          toAlbum:(NSString *)albumName
       completion:(ALAssetsLibraryWriteImageCompletionBlock)completion
          failure:(ALAssetsLibraryAccessFailureBlock)failure;

- (void)saveVideo:(NSURL *)videoUrl
          toAlbum:(NSString *)albumName
       completion:(ALAssetsLibraryWriteImageCompletionBlock)completion
          failure:(ALAssetsLibraryAccessFailureBlock)failure;

- (void)loadImagesFromAlbum:(NSString *)albumName
                 completion:(void (^)(NSMutableArray *images, NSError *error))completion;
@end


@interface ALAssetsLibrary (Private)

/*! A block wraper to be executed after asset adding process done. (Private)
 *
 * \param albumName Custom album name
 * \param completion Block to be executed when succeed to add the asset to the assets library (camera roll)
 * \param failure Block to be executed when failed to add the asset to the custom photo album
 */
- (ALAssetsLibraryWriteImageCompletionBlock)_resultBlockOfAddingToAlbum:(NSString *)albumName
                                                             completion:(ALAssetsLibraryWriteImageCompletionBlock)completion
                                                                failure:(ALAssetsLibraryAccessFailureBlock)failure;

/*! A block wraper to be executed after |-assetForURL:resultBlock:failureBlock:| succeed.
 *  Generally, this block will be excused when user confirmed the application's access
 *    to the library.
 *
 * \param group A group to be used to add photo to the target album
 * \param assetURL The URL for the target asset
 * \param completion Block to be executed when succeed to add the asset to the assets library (camera roll)
 * \param failure Block to be executed when failed to add the asset to the custom photo album
 *
 * \return An ALAssetsLibraryAssetForURLResultBlock type block
 */
- (ALAssetsLibraryAssetForURLResultBlock)_assetForURLResultBlockWithGroup:(ALAssetsGroup *)group
                                                                 assetURL:(NSURL *)assetURL
                                                               completion:(ALAssetsLibraryWriteImageCompletionBlock)completion
                                                                  failure:(ALAssetsLibraryAccessFailureBlock)failure;

@end

@implementation IFPhotoManager

+ (ALAssetsLibrary *)defaultAssetsLibrary {
    static dispatch_once_t pred = 0;
    static ALAssetsLibrary *library = nil;
    dispatch_once(&pred, ^{
        library = [[ALAssetsLibrary alloc] init];
    });
    return library;
}


- (void) saveImage:(UIImage *)image
     toCustomAlbum:(NSString *)albumName
 completionHandler:(nullable void(^)(BOOL success, NSError *__nullable error))completionHandler {
    
    if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        
        ALAssetsLibrary *assetLib = [[ALAssetsLibrary alloc] init];
        [assetLib saveImage:image toAlbum:albumName completion:^(NSURL *assetURL, NSError *error) {
            completionHandler(assetURL ? YES : NO, error);
        } failure:^(NSError *error) {
            completionHandler(NO, error);
        }];
        
    } else {
        
        [self addImage:image toCustomAlbum:albumName completionHandler:^(BOOL success, NSError * _Nullable error) {
            completionHandler(success, error);
        }];
    }
}

- (void) saveVideo:(NSString *)videoUrl
     toCustomAlbum:(NSString *)albumName
 completionHandler:(nullable void(^)(BOOL success, NSError *__nullable error))completionHandler {

    if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        
        ALAssetsLibrary *assetLib = [[ALAssetsLibrary alloc] init];
        [assetLib saveVideo:[NSURL URLWithString:videoUrl] toAlbum:albumName completion:^(NSURL *assetURL, NSError *error) {
            completionHandler(assetURL ? YES : NO, error);
        } failure:^(NSError *error) {
           completionHandler(NO, error);
        }];
    } else {
        
        [self addVideo:videoUrl toCustomAlbum:albumName completionHandler:^(BOOL success, NSError * _Nullable error) {
           completionHandler(success, error);
        }];
    }
}

- (void) addImage:(UIImage *)image
    toCustomAlbum:(NSString *)albumName
completionHandler:(nullable void(^)(BOOL success, NSError *__nullable error))completionHandler {
    
    // Create the new album.
    __block PHObjectPlaceholder *myAlbum;
    
    PHFetchOptions *fetchOptions = [[PHFetchOptions alloc] init];
    fetchOptions.predicate = [NSPredicate predicateWithFormat:@"title = %@", albumName];
    PHAssetCollection *assetCollection = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum
                                                                                  subtype:PHAssetCollectionSubtypeAny
                                                                                  options:fetchOptions].firstObject;
    // If album exists then write inside that.
    if (assetCollection) {
        [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
            PHAssetChangeRequest *assetChangeRequest = [PHAssetChangeRequest creationRequestForAssetFromImage:image];
            
            // add asset
            PHAssetCollectionChangeRequest *assetCollectionChangeRequest = [PHAssetCollectionChangeRequest changeRequestForAssetCollection:assetCollection];
            [assetCollectionChangeRequest addAssets:@[[assetChangeRequest placeholderForCreatedAsset]]];
        } completionHandler:^(BOOL success, NSError *error) {
            completionHandler(success, error);
        }];
    } else {
        
        [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
            PHAssetCollectionChangeRequest *changeRequest = [PHAssetCollectionChangeRequest creationRequestForAssetCollectionWithTitle:albumName];
            myAlbum = changeRequest.placeholderForCreatedAssetCollection;
        } completionHandler:^(BOOL success, NSError *error) {
            if (success) {
                PHFetchResult *fetchResult = [PHAssetCollection fetchAssetCollectionsWithLocalIdentifiers:@[myAlbum.localIdentifier] options:nil];
                PHAssetCollection *assetCollection = fetchResult.firstObject;
                
                [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
                    PHAssetChangeRequest *assetChangeRequest = [PHAssetChangeRequest creationRequestForAssetFromImage:[UIImage imageNamed:@"14339210_10209844609849242_202064225_o.jpg"]];
                    
                    // add asset
                    PHAssetCollectionChangeRequest *assetCollectionChangeRequest = [PHAssetCollectionChangeRequest changeRequestForAssetCollection:assetCollection];
                    [assetCollectionChangeRequest addAssets:@[[assetChangeRequest placeholderForCreatedAsset]]];
                } completionHandler:^(BOOL success, NSError *error) {
                    completionHandler(success, error);
                }];
            } else {
                completionHandler(success, error);
            }
        }];
    }
}

- (void) addVideo:(NSString *)videoUrl
    toCustomAlbum:(NSString *)albumName
completionHandler:(nullable void(^)(BOOL success, NSError *__nullable error))completionHandler {
    
    // Create the new album.
    __block PHObjectPlaceholder *myAlbum;
    
    PHFetchOptions *fetchOptions = [[PHFetchOptions alloc] init];
    fetchOptions.predicate = [NSPredicate predicateWithFormat:@"title = %@", albumName];
    PHAssetCollection *assetCollection = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum
                                                                                  subtype:PHAssetCollectionSubtypeAny
                                                                                  options:fetchOptions].firstObject;
    // If album exists then write inside that.
    if (assetCollection) {
        [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
            PHAssetChangeRequest *assetChangeRequest = [PHAssetChangeRequest creationRequestForAssetFromVideoAtFileURL:[NSURL URLWithString:videoUrl]];
            
            // add asset
            PHAssetCollectionChangeRequest *assetCollectionChangeRequest = [PHAssetCollectionChangeRequest changeRequestForAssetCollection:assetCollection];
            [assetCollectionChangeRequest addAssets:@[[assetChangeRequest placeholderForCreatedAsset]]];
        } completionHandler:^(BOOL success, NSError *error) {
            completionHandler(success, error);
        }];
    } else {
        
        [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
            PHAssetCollectionChangeRequest *changeRequest = [PHAssetCollectionChangeRequest creationRequestForAssetCollectionWithTitle:albumName];
            myAlbum = changeRequest.placeholderForCreatedAssetCollection;
        } completionHandler:^(BOOL success, NSError *error) {
            if (success) {
                PHFetchResult *fetchResult = [PHAssetCollection fetchAssetCollectionsWithLocalIdentifiers:@[myAlbum.localIdentifier] options:nil];
                PHAssetCollection *assetCollection = fetchResult.firstObject;
                
                [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
                    PHAssetChangeRequest *assetChangeRequest = [PHAssetChangeRequest creationRequestForAssetFromImage:[UIImage imageNamed:@"14339210_10209844609849242_202064225_o.jpg"]];
                    
                    // add asset
                    PHAssetCollectionChangeRequest *assetCollectionChangeRequest = [PHAssetCollectionChangeRequest changeRequestForAssetCollection:assetCollection];
                    [assetCollectionChangeRequest addAssets:@[[assetChangeRequest placeholderForCreatedAsset]]];
                } completionHandler:^(BOOL success, NSError *error) {
                    completionHandler(success, error);
                }];
            } else {
                completionHandler(success, error);
            }
        }];
    }
}

- (void)getAssetsInAlbum:(NSString *)albumName
       completionHandler:(nullable void(^)(NSMutableArray *assetArray, NSMutableArray *contentArray, NSError *__nullable error))completionHandler {

    if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        
        ALAssetsLibrary *assetLib = [IFPhotoManager defaultAssetsLibrary];
        [assetLib loadImagesFromAlbum:albumName completion:^(NSMutableArray * _Nonnull images, NSError *__nullable error) {
            completionHandler(images, nil, error);
        }];
        
    } else {
        
        [self getImagesInAlbumForiOS8Above:albumName
         completionHandler:completionHandler];
    }
}

-(void)getImagesInAlbumForiOS8Above:(NSString *)albumName
                  completionHandler:(nullable void(^)(NSMutableArray *assetArr, NSMutableArray *contentArr, NSError *__nullable error))completionHandler {
    
    __block PHAssetCollection *collection;
    
    // Find the album
    PHFetchOptions *fetchOptions = [[PHFetchOptions alloc] init];
    fetchOptions.predicate = [NSPredicate predicateWithFormat:@"title = %@", albumName];
    collection = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum
                                                          subtype:PHAssetCollectionSubtypeAny
                                                          options:fetchOptions].firstObject;
    
    PHFetchResult *collectionResult = [PHAsset fetchAssetsInAssetCollection:collection options:nil];
    __block NSMutableArray *assetArray = [NSMutableArray array];

    // Try smart albums now.
    if (collectionResult.count == 0) {
        
        //set up fetch options, mediaType is image.
        PHFetchOptions *options = [[PHFetchOptions alloc] init];
        options.predicate = [NSPredicate predicateWithFormat:@"title = %@", albumName];
        PHFetchResult *smartAlbums = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum subtype:PHAssetCollectionSubtypeAny options:nil];
        
        for (NSInteger i =0; i < smartAlbums.count; i++) {
            PHAssetCollection *assetCollection = smartAlbums[i];
            PHFetchResult *assetsFetchResult = [PHAsset fetchAssetsInAssetCollection:assetCollection options:nil];
            
            NSLog(@"sub album title is %@, count is %ld", assetCollection.localizedTitle, assetsFetchResult.count);
            if ([assetCollection.localizedTitle isEqualToString:albumName] && assetsFetchResult.count > 0) {
                for (PHAsset *asset in assetsFetchResult) {
                    [assetArray addObject:asset];
                }
                completionHandler(assetArray, nil, nil);
            }
        }
    }
    
    // Get total count.
    __block NSInteger loopedCount = collectionResult.count;
    [collectionResult enumerateObjectsUsingBlock:^(PHAsset *asset, NSUInteger idx, BOOL *stop) {
        
        [assetArray addObject:asset];
        loopedCount--;
        if (loopedCount == 0 && completionHandler)
            completionHandler(assetArray, nil, nil);
    }];
}

- (void)deletePhoto:(PHAsset *)asset withCompletionHandler:(nullable void(^)(BOOL success, NSError *__nullable error))completionHandler {
    
    if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        NSString * message = @"Failed to delete photo-Delete not suppored for iOS7";
        if (completionHandler)
            completionHandler(NO, [NSError errorWithDomain:@"IFManager-Failed-Delete"
                                                code:0
                                            userInfo:@{NSLocalizedDescriptionKey : message}]);
    } else {
        [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
            [PHAssetChangeRequest deleteAssets:@[asset]];
        } completionHandler:^(BOOL success, NSError * _Nullable error) {
            if (error)
                NSLog(@"Error: %@", error);
            dispatch_async(dispatch_get_main_queue(), ^{
                completionHandler(success, error);
            });
        }];
    }
}

@end

@implementation ALAssetsLibrary (CustomPhotoAlbum)
#pragma mark iOS7 Methods

- (ALAssetsLibraryWriteImageCompletionBlock)_resultBlockOfAddingToAlbum:(NSString *)albumName
                                                             completion:(ALAssetsLibraryWriteImageCompletionBlock)completion
                                                                failure:(ALAssetsLibraryAccessFailureBlock)failure
{
    return ^(NSURL *assetURL, NSError *error) {
        // Run the completion block for writing image to saved
        //   photos album
        //if (completion) completion(assetURL, error);
        
        // If an error occured, do not try to add the asset to
        //   the custom photo album
        if (error != nil) {
            if (failure) failure(error);
            return;
        }
        
        // Add the asset to the custom photo album
        [self addAssetURL:assetURL
                  toAlbum:albumName
               completion:completion
                  failure:failure];
    };
}

- (ALAssetsLibraryAssetForURLResultBlock)_assetForURLResultBlockWithGroup:(ALAssetsGroup *)group
                                                                 assetURL:(NSURL *)assetURL
                                                               completion:(ALAssetsLibraryWriteImageCompletionBlock)completion
                                                                  failure:(ALAssetsLibraryAccessFailureBlock)failure
{
    return ^(ALAsset *asset) {
        // Add photo to the target album
        if ([group addAsset:asset]) {
            // Run the completion block if the asset was added successfully
            if (completion)
                completion(assetURL, nil);
        }
        // |-addAsset:| may fail (return NO) if the group is not editable,
        //   or if the asset could not be added to the group.
        else {
            NSString * message = [NSString stringWithFormat:@"ALAssetsGroup failed to add asset: %@.", asset];
            if (completion)
                completion(nil, [NSError errorWithDomain:@"LIB_ALAssetsLibrary_CustomPhotoAlbum"
                                                    code:0
                                                userInfo:@{NSLocalizedDescriptionKey : message}]);
        }
    };
}

#pragma mark - Public Method

- (void)saveImage:(UIImage *)image
          toAlbum:(NSString *)albumName
       completion:(ALAssetsLibraryWriteImageCompletionBlock)completion
          failure:(ALAssetsLibraryAccessFailureBlock)failure
{
    [self writeImageToSavedPhotosAlbum:image.CGImage
                           orientation:(ALAssetOrientation)image.imageOrientation
                       completionBlock:[self _resultBlockOfAddingToAlbum:albumName
                                                              completion:completion
                                                                 failure:failure]];
}

- (void)saveVideo:(NSURL *)videoUrl
          toAlbum:(NSString *)albumName
       completion:(ALAssetsLibraryWriteImageCompletionBlock)completion
          failure:(ALAssetsLibraryAccessFailureBlock)failure
{
    [self writeVideoAtPathToSavedPhotosAlbum:videoUrl
                             completionBlock:[self _resultBlockOfAddingToAlbum:albumName
                                                                    completion:completion
                                                                       failure:failure]];
}

- (void)addAssetURL:(NSURL *)assetURL
            toAlbum:(NSString *)albumName
         completion:(ALAssetsLibraryWriteImageCompletionBlock)completion
            failure:(ALAssetsLibraryAccessFailureBlock)failure
{
    __block BOOL albumWasFound = NO;
    
    // Signature for the block executed when a match is found during enumeration using
    //   |-enumerateGroupsWithTypes:usingBlock:failureBlock:|.
    //
    // |group|: The current asset group in the enumeration.
    // |stop| : A pointer to a boolean value; set the value to YES to stop enumeration.
    //
    ALAssetsLibraryGroupsEnumerationResultsBlock enumerationBlock;
    enumerationBlock = ^(ALAssetsGroup *group, BOOL *stop) {
        // Compare the names of the albums
        if ([albumName compare:[group valueForProperty:ALAssetsGroupPropertyName]] == NSOrderedSame) {
            // Target album is found
            albumWasFound = YES;
            
            // Get a hold of the photo's asset instance
            // If the user denies access to the application, or if no application is allowed to
            //   access the data, the failure block is called.
            ALAssetsLibraryAssetForURLResultBlock assetForURLResultBlock =
            [self _assetForURLResultBlockWithGroup:group
                                          assetURL:assetURL
                                        completion:completion
                                           failure:failure];
            [self assetForURL:assetURL
                  resultBlock:assetForURLResultBlock
                 failureBlock:failure];
            
            // Album was found, bail out of the method
            *stop = YES;
        }
        
        if (group == nil && albumWasFound == NO) {
            // Photo albums are over, target album does not exist, thus create it
            
            // Since you use the assets library inside the block,
            //   ARC will complain on compile time that there’s a retain cycle.
            //   When you have this – you just make a weak copy of your object.
            ALAssetsLibrary * __weak weakSelf = self;
            
            void(^addPhotoToLibraryBlock)(ALAssetsGroup *group) = ^void(ALAssetsGroup *group) {
                // Get the photo's instance
                //   add the photo to the newly created album
                ALAssetsLibraryAssetForURLResultBlock assetForURLResultBlock =
                [weakSelf _assetForURLResultBlockWithGroup:group
                                                  assetURL:assetURL
                                                completion:completion
                                                   failure:failure];
                [weakSelf assetForURL:assetURL
                          resultBlock:assetForURLResultBlock
                         failureBlock:failure];
            };
            
            // If iOS version is lower than 5.0, throw a warning message
            if (! [self respondsToSelector:@selector(addAssetsGroupAlbumWithName:resultBlock:failureBlock:)]) {
                NSLog(@"%s: WARNING: |-addAssetsGroupAlbumWithName:resultBlock:failureBlock:| \
                      only available on iOS 5.0 or later. Asset cannot be saved to album.", __PRETTY_FUNCTION__);
            } else {
                
                // code that always creates an album on iOS 7.x.x but fails
                // in certain situations such as if album has been deleted
                // previously on iOS 8.x.
                [self addAssetsGroupAlbumWithName:albumName
                                      resultBlock:addPhotoToLibraryBlock
                                     failureBlock:failure];
                
            }
            // Should be the last iteration anyway, but just in case
            *stop = YES;
        }
    };
    
    // Search all photo albums in the library
    [self enumerateGroupsWithTypes:ALAssetsGroupAlbum
                        usingBlock:enumerationBlock
                      failureBlock:failure];
}

- (void)loadImagesFromAlbum:(NSString *)albumName
                 completion:(void (^)(NSMutableArray *, NSError *))completion
{
    ALAssetsLibraryGroupsEnumerationResultsBlock block = ^(ALAssetsGroup *group, BOOL *stop) {
        // Checking if library exists
        if (group == nil) {
            *stop = YES;
            return;
        }
        
        // If we have found library with given title we enumerate it
        if ([albumName compare:[group valueForProperty:ALAssetsGroupPropertyName]] == NSOrderedSame) {
            NSMutableArray * images = [[NSMutableArray alloc] init];
            [group enumerateAssetsUsingBlock:^(ALAsset *result, NSUInteger index, BOOL *stop) {
                // Checking if group isn't empty
                if (! result) return;
                
                if ([[result valueForProperty:ALAssetPropertyType] isEqualToString:ALAssetTypeVideo]) {
                    // asset is a video
                    [images addObject:result];
                } else {
                    // Getting the image from the asset
                    UIImageOrientation orientation =
                    (UIImageOrientation)[[result valueForProperty:@"ALAssetPropertyOrientation"] intValue];
                    UIImage *image = [UIImage imageWithCGImage:[[result defaultRepresentation] fullScreenImage]
                                                         scale:1.0
                                                   orientation:orientation];
                    // Saving this image to the array
                    [images addObject:image];
                }
                
            }];
            
            // Execute the |completion| block
            if (completion) completion(images, nil);
            
            // Album was found, bail out of the method
            *stop = YES;
        }
    };
    
    ALAssetsLibraryAccessFailureBlock failureBlock = ^(NSError *error) {
        NSLog(@"%s: %@", __PRETTY_FUNCTION__, [error localizedDescription]);
        if (completion) completion(nil, error);
    };
    
    [self enumerateGroupsWithTypes:ALAssetsGroupAll
                        usingBlock:block
                      failureBlock:failureBlock];
}

@end
